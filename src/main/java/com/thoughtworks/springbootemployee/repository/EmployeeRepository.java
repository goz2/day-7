package com.thoughtworks.springbootemployee.repository;

import com.thoughtworks.springbootemployee.entity.Employee;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Repository
public class EmployeeRepository {
    private List<Employee> employees = new ArrayList<>();

    public EmployeeRepository() {
    }

    public EmployeeRepository(List<Employee> employees) {
        this.employees = employees;
    }

    public List<Employee> getEmployeeList() {
        return employees;
    }

    public Employee getEmployeeById(int id) {
        return employees.stream().filter(data -> data.getId() == id).findFirst().orElse(null);
    }

    public List<Employee> getEmployeesByGender(String gender) {
        return employees.stream().filter(employee -> gender.equals(employee.getGender())).collect(Collectors.toList());
    }

    public Integer createEmployee(Employee employee) {
        employee.setId(generateId());
        employees.add(employee);
        return employee.getId();
    }

    public Employee updateEmployee(int id, Employee employee) {
        Employee currentEmployee = employees.stream()
                .filter(employee1 -> employee1.getId() == id)
                .findFirst().orElse(null);
        if (currentEmployee == null) {
            return null;
        }
        currentEmployee.setAge(employee.getAge());
        currentEmployee.setSalary(employee.getSalary());
        return currentEmployee;
    }

    public int deleteEmployees(int id) {
        Employee employee1 = employees.stream().filter(employee -> employee.getId() == id).findFirst().orElse(null);
        if (employee1 == null) {
            return 0;
        }
        employees.remove(employee1);
        return employee1.getId();
    }

    public List<Employee> getEmployeeListByPage(int page, int size) {
        return employees.stream().skip((long) (page - 1) * size).limit(size).collect(Collectors.toList());
    }

    private Integer generateId() {
        int maxId = employees.stream().mapToInt(Employee::getId).max().orElse(0);
        return maxId + 1;
    }
}
