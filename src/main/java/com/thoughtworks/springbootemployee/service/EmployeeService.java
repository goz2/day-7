package com.thoughtworks.springbootemployee.service;

import com.thoughtworks.springbootemployee.entity.Employee;

import java.util.List;

public interface EmployeeService {
    List<Employee> getEmployeeList();

    Employee getEmployeeById(int id);

    List<Employee> getEmployeesByGender(String gender);

    Integer createEmployee(Employee employee);

    Employee updateEmployee(int id, Employee employee);

    int deleteEmployees(int id);

    List<Employee> getEmployeeListByPage(int page, int size);
}
